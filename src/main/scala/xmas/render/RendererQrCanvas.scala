package xmas.render

// Auxilliary class for display purpose. Work with Renderer.scala

import java.awt.Graphics2D
import scala.swing.Panel
import xmas.aux.Board

class RenderQrCanvas(board: Board, blockSize: Int) extends Panel {
    var currentBoard = board

    override def paintComponent(g: Graphics2D) {
        currentBoard.draw(g, blockSize)
    }

    def setBoard(board: Board) = {
        currentBoard = board
        repaint()
    }
}

