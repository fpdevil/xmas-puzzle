package xmas.render

// Auxilliary class for the Display
// Using the javax.swing & AWT
// Reference http://otfried.org/scala/gui.html

import scala.swing.MainFrame
import scala.swing.Dialog
import xmas.aux.Board

class Renderer(blockSize: Int, board: Board) extends MainFrame {
  title = "XMAS Puzzle Solution ???"
  

  import javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE
  peer.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE)
  // peer.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE)

  override def closeOperation() = {
    showCloseDialog()
  }

  private def showCloseDialog() {
    Dialog.showConfirmation(parent  = null,
                            title   = "Exit",
                            message = "Close the Window ?"
    ) match {
      // case Dialog.Result.Ok => super.closeOperation()
      case Dialog.Result.Ok => sys.exit(0)
      case _                => ()
    }
  }
  // specify the dimensions for output
  preferredSize = new java.awt.Dimension((board.size * blockSize) + 20, (board.size * blockSize) + 40)
  val panel = new RenderQrCanvas(board, blockSize)
  contents = panel
  def setBoard(board: Board) = {
    panel.setBoard(board)
  }
}
