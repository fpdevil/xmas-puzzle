package xmas.aux

import java.awt.{Graphics2D, Color}

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Board(board: Seq[Seq[Boolean]]) {
  val log = LoggerFactory.getLogger(this.getClass)

  val rows    = board.toList
  val columns = board.transpose.toList
  val size    = rows.length

  log.info(s"\n rows: ${rows} \n columns: ${columns}")

  def cell(x: Int, y: Int) = board(x)(y)

  def isValidFor(rowSpec: Seq[Seq[Int]], columnSpec: Seq[Seq[Int]]): Boolean = {
    log.info(s"isValidFor rowSpec: ${rowSpec}")
    log.info(s"isValidFor columnSpec: ${columnSpec}")

    def toSpec(actual: List[Seq[Boolean]]) = {
      log.info(s"Conversion of Boolean List ... \n ${actual}")
      actual.map(AuxHelper.singleToSpec).toList
    }
    rowSpec == toSpec(rows) && columnSpec == toSpec(columns)
  }

  def draw(g: Graphics2D, blockSize: Int) = {
    // clear background
    g.setColor(Color.WHITE)
    g.fillRect(0, 0, size * blockSize, size * blockSize)

    for {
      row <- 0 until size
      col <- 0 until size
    } {
      if (cell(row, col)) g.setColor(Color.BLACK)
      else g.setColor(Color.WHITE)
      g.fillRect(col * blockSize, row * blockSize, blockSize, blockSize)
    }
  }

  override def equals(o: Any) = o match {
    case that: Board => that.toString == toString
    case _           => false
  }

  // override the toString for display purpose
  override lazy val toString = rows.map(row => ("|" :: row.map(if (_) "*|" else " |").toList).mkString("")).mkString("\n")
}
