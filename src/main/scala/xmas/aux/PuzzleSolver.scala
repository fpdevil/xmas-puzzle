package xmas.aux;

import org.slf4j.Logger
import org.slf4j.LoggerFactory

object PuzzleSolver {
   val log = LoggerFactory.getLogger(this.getClass)

  def and(a: Seq[Boolean], b: Seq[Boolean]) = {
    log.debug(s"invoking and with a: ${a} and b: ${b}")
    a zip b map { case (aval, bval) => aval & bval }
  }

  def andOverSeq(seqs: Seq[Seq[Boolean]]): Seq[Boolean] = {
    log.debug(s"andOverSeq : ${seqs}")
    /**
     andOverSeq : Vector(List(true, false, true, false, false, true, true, false, true, false, true, false, true, true, 
     false, false, false, false, false, false, false, false, false, false, false), List(true,
     */
    seqs.transpose.map(_.forall(x => x))
  }

  def allPossiblePermutationsOf(specs: Seq[Int], currentDefiniteBlocks: Seq[Boolean]): Seq[Seq[Boolean]] = {

    log.debug(s"Permutations specifications: ${specs}, currentDefiniteBlocks: ${currentDefiniteBlocks}")
    /**
     Permutations specifications: List(2, 1, 1), 
     currentDefiniteBlocks: List(false, false, true, false, false, true, false, false, false)
     Permutations specifications: List(1), 
     currentDefiniteBlocks: List(false, false, false, true, false, false, false, false, false, true)
     */

    def minSize(specs: Seq[Int]) = {
      specs.sum + specs.length - 1
      }

    if (AuxHelper.singleToSpec(currentDefiniteBlocks) == specs) List(currentDefiniteBlocks)
    else if (currentDefiniteBlocks.length == 0 && specs.length == 0) List(Nil)
    else if (specs.length == 0) List(List.fill(currentDefiniteBlocks.length)(false))
    else {
      if (currentDefiniteBlocks.length < minSize(specs)) Nil
      else {
        val firstSpec :: theRest = specs
        val mySpace = if (theRest == Nil) {
            currentDefiniteBlocks.length
          } else {
            currentDefiniteBlocks.length - minSize(theRest) - 1
          }
        val myBlock = List.fill(firstSpec)(true)
        val myPossibilities = for (prefix <- 0 to mySpace - firstSpec) yield List.fill(prefix)(false) ++ myBlock

        log.debug(s"myBlock: ${myBlock} | myPossibilities: ${myPossibilities}")
        /**
         myBlock: List(true)
         myPossibilities: Vector(List(true), List(false, true), List(false, false, true), List(false, false, false, true), List(false, false, false, false, true), List(false, false, false, false, false, true), List(false, false, false, false, false, false, true), List(false, false, false, false, false, false, false, true), List(false, false, false, false, false, false, false, false, true))
         */

        val everything = for {
          possiblityPrefix  <- myPossibilities
          suffixPossibility <- allPossiblePermutationsOf(theRest, currentDefiniteBlocks.drop(possiblityPrefix.length + 1))
        } yield {
          if (suffixPossibility.length > 0) possiblityPrefix ++ List(false) ++ suffixPossibility
          else possiblityPrefix ++ List.fill(mySpace - possiblityPrefix.length)(false)
        }

        val allValid = for {
          possiblity <- everything
          if (and(possiblity, currentDefiniteBlocks).toList == currentDefiniteBlocks.toList)
        } yield possiblity

        allValid
      }
    }
  }

}

class PuzzleSolver(progressReporter: (Board) => Unit) {
  val log = LoggerFactory.getLogger(this.getClass)

  private def getDefiniteBlocks(definiteBlocks: Seq[Seq[Boolean]], specs: Seq[Seq[Int]]) = {
    log.info(s"definite blocks: ${definiteBlocks}")
    definiteBlocks zip specs map { 
      case (blocks, spec) => PuzzleSolver.andOverSeq(PuzzleSolver.allPossiblePermutationsOf(spec, blocks)) 
    }
  }

  private def solverStep(board: Board, rowSpec: Seq[Seq[Int]], columnSpec: Seq[Seq[Int]]) = {
    log.info(s"solver steps for board \n ${board}, \n rowSpec: ${rowSpec}, \n colSpec: ${columnSpec}")
    val getDefiniteFromRows = (board: Board) => {
      new Board(getDefiniteBlocks(board.rows, rowSpec))
    }

    val getDefiniteFromColumns = (board: Board) => {
      new Board(getDefiniteBlocks(board.columns, columnSpec).transpose)
    }

    val getDefiniteFromRowsAndColumns = (board: Board) => {
      getDefiniteFromColumns(getDefiniteFromRows(board))
    }

    val getDefiniteFromColumnsBasedOnThoseRuledOutByRows = (board: Board) => {
      val deffoNotFromRows = board.rows zip rowSpec map {
        case (row, spec) => {
          val all     = PuzzleSolver.allPossiblePermutationsOf(spec, row)
          val inverse = all.map(_.map(x => !x))
          PuzzleSolver.andOverSeq(inverse)
        }
      }

      val newFilteredColumns = board.columns zip columnSpec zip deffoNotFromRows.transpose map {
        case ((column, spec), deffoNot) => {
          val all      = PuzzleSolver.allPossiblePermutationsOf(spec, column)
          val filtered = all filter (perm => perm zip deffoNot forall { case (poss, deffoNot) => !(poss && deffoNot) })
          PuzzleSolver.andOverSeq(filtered)
        }
      }

      new Board(newFilteredColumns.transpose)
    }
    val solvingTactics = Stream(getDefiniteFromRowsAndColumns, getDefiniteFromColumnsBasedOnThoseRuledOutByRows)
    solvingTactics map (_(board)) find (_ != board)
  }

  def solve(board: Board, rowSpec: Seq[Seq[Int]], columnSpec: Seq[Seq[Int]]): Board = {
    log.info(s"## Solving for Board: \n${board}")
    log.info(s"## rowSpec: \n${rowSpec}")
    log.info(s"## columnSpec: \n${columnSpec}")
    progressReporter(board)
    if (board.isValidFor(rowSpec, columnSpec)) board
    else {
      val newBoard = solverStep(board, rowSpec, columnSpec)
      newBoard match {

        case Some(newBoard) => solve(newBoard, rowSpec, columnSpec)
        case None           => throw new Exception("cannot solve puzzle")
      }
    }
  }
}
