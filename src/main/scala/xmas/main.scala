package xmas

import java.awt.image.BufferedImage
import scala.io.Source
import xmas.aux.{Board, AuxHelper, PuzzleSolver}
import xmas.render.Renderer

import org.slf4j.Logger
import org.slf4j.LoggerFactory

object Main {
  val log = LoggerFactory.getLogger(this.getClass)

  val blockSize = 10;

  def saveImageOf(board: Board) = {
    val size   = board.size
    val canvas = new BufferedImage(size * blockSize, size * blockSize, BufferedImage.TYPE_INT_RGB)

    val g = canvas.createGraphics()

    board.draw(g, blockSize)

    javax.imageio.ImageIO.write(canvas, "png", new java.io.File("solution.png"))
  }

  def main(args: Array[String]) {
    // val input = Source.fromFile("input.txt", "utf-8").getLines.toList
    // http://stackoverflow.com/questions/27360977/how-to-read-files-from-resources-folder-in-scala
    val stream     = getClass.getResourceAsStream("/input.txt")
    val input      = Source.fromInputStream( stream ).getLines.toList
    val columnSpec = (input take 25).map(spec => spec.split(" ").toList.map(_.toInt)).toList
    val rowSpec    = (input drop 26 take 25).map(spec => spec.split(" ").toList.map(_.toInt)).toList
    val blocks     = input drop 52
    /**
     In the beginning all unknown values are empty which are denoted as False
     The given clues which are pre-filled values are all True. After the iterations
     the False values wil change gradually to True
     */
    log.info(s"## Input details from file: ${input}")
    log.info(s"## columnSpec: ${columnSpec}")
    log.info(s"## rowSpec: ${rowSpec}")
    log.info(s"## provided clues: ${blocks}")

    var board = AuxHelper.createBoard(25, blocks.toList)
    log.info(s"## Board details: \n${board}")
    val ui = new Renderer(blockSize, board)
    ui.visible = true

    val uiDisplayBoard = (x: Board) => { ui.setBoard(x) }

    val solver = new PuzzleSolver(uiDisplayBoard).solve(board, rowSpec, columnSpec)
    saveImageOf(solver)
  }
}
