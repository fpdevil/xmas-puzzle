name := "xmas-puzzle"

val slf4japi = "org.slf4j" % "slf4j-api" % "1.7.5"
val slf4j = "org.slf4j" % "slf4j-simple" % "1.7.5"
val logback = "ch.qos.logback" % "logback-classic" % "1.0.3"
val swing = "org.scala-lang" % "scala-swing" % "2.11+"
val scalatest = "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"

lazy val root = (project in file(".")).
    settings(
        name := "xmas puzzle",
        scalaVersion := "2.11.8",
        libraryDependencies ++= Seq(slf4japi, slf4j, logback, swing, scalatest)
    )
